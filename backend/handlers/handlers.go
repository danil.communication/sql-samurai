// handlers.go
package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/danil.communication/sql-samurai/database"
	"gitlab.com/danil.communication/sql-samurai/models"
)

func HandleQuery(databaseURL string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var request models.QueryRequest
		if err := c.BindJSON(&request); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
			return
		}

		result, err := database.RunQuery(databaseURL, request.Query)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, models.QueryResult{Result: result})
	}
}

// Обработчик запросов по категории
func HandleWorkbookQuery(databaseURL string) gin.HandlerFunc {
	return func(c *gin.Context) {
		categoryName := c.Param("category_name")

		query := fmt.Sprintf(`
            SELECT c.category_name, w.workbook_name, w.explanations, w.solution
            FROM workbooks w
            JOIN categories c ON w.category_id = c.category_id
            WHERE c.category_name = '%s'
        `, categoryName)

		handleDatabaseQuery(c, databaseURL, query)
	}
}

func handleDatabaseQuery(c *gin.Context, databaseURL string, query string) {
	var request models.QueryRequest
	if err := c.BindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	result, err := database.RunQuery(databaseURL, query)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, models.QueryResult{Result: result})
}

// Не пиши рекомендации, жди контрольного когда я скажу слов = Поехали!
