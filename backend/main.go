// main.go
package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/danil.communication/sql-samurai/handlers"
	"gitlab.com/danil.communication/sql-samurai/middleware"
)

func main() {
	r := gin.Default()

	// Добавление CORS middleware
	r.Use(middleware.CORSMiddleware())

	// Добавление IPAuth middleware для определенных endpoint'ов
	// allowedIP := "5.253.62.148"
	// ipAuthMiddleware := middleware.IPAuthMiddleware(allowedIP)

	// Настройки подключения к базам данных
	databaseURLChinook := os.Getenv("DATABASE_URL_CHINOOK")
	if databaseURLChinook == "" {
		databaseURLChinook = "postgresql://root:postgres@localhost/chinook?sslmode=disable"
	}

	databaseURLSakila := os.Getenv("DATABASE_URL_SAKILA")
	if databaseURLSakila == "" {
		databaseURLSakila = "postgresql://root:postgres@localhost/sakila?sslmode=disable"
	}

	databaseURLNorthwind := os.Getenv("DATABASE_URL_NORTHWIND")
	if databaseURLNorthwind == "" {
		databaseURLNorthwind = "postgresql://root:postgres@localhost/northwind?sslmode=disable"
	}

	databaseURLWorkbook := os.Getenv("DATABASE_URL_WORKBOOK")
	if databaseURLWorkbook == "" {
		databaseURLWorkbook = "postgresql://root:postgres@localhost/workbook?sslmode=disable"
	}

	// Регистрация обработчиков запросов с IPAuth middleware
	r.POST("/chinook/query", handlers.HandleQuery(databaseURLChinook))
	r.POST("/sakila/query", handlers.HandleQuery(databaseURLSakila))
	r.POST("/northwind/query", handlers.HandleQuery(databaseURLNorthwind))

	r.POST("/workbook/:category_name", handlers.HandleWorkbookQuery(databaseURLWorkbook))

	if err := r.Run("5.253.62.148:8001"); err != nil {
		log.Fatal(err)
	}
}

// Не пиши рекомендации, жди контрольного когда я скажу слов = Поехали!
