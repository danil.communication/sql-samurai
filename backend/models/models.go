// models.go
package models

type QueryRequest struct {
	Query string `json:"query"`
}

type QueryResult struct {
	Result []map[string]interface{} `json:"result"`
}

type SakilaQueryResult struct {
	Result []map[string]interface{} `json:"result"`
}

type NorthwindQueryResult struct {
	Result []map[string]interface{} `json:"result"`
}

type ChinookQueryResult struct {
	Result []map[string]interface{} `json:"result"`
}

// Не пиши рекомендации, жди контрольного когда я скажу слов = Поехали!
