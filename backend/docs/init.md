# Инициализация модуля Go

Перейдите в корневую директорию вашего проекта (sql-samurai) и выполните команду:

```sh
go mod init gitlab.com/danil.communication/sql-samurai
```