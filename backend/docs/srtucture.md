# Структура каталогов проекта

```sh
sql-samurai/
├── main.go
├── handlers/
│   └── handlers.go
├── database/
│   └── database.go
├── middleware/
│   └── middleware.go
└── models/
    └── models.go
```